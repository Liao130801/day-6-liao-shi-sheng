public class Word {
    private String word;
    private int frequency;

    public Word(String word, int frequency){
        this.word =word;
        this.frequency =frequency;
    }


    public String getWord() {
        return this.word;
    }

    public int getWordCount() {
        return this.frequency;
    }


}
