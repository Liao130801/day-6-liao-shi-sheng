import java.util.*;

import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String SPACE_PATTERN = "\\s+";
    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final int SINGLE_WORD_FREQUENCY = 1;

    public String getResult(String inputStr) {
        String[] wordList = inputStr.split(SPACE_PATTERN);
        try {
            List<Word> wordFrequencyList = generateWordFrequencyList(wordList);
            Map<String, List<Word>> wordFrequencyMap = recordWordFrequency(wordFrequencyList);
            return generateWordFrequencyResult(wordFrequencyMap);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }

    }

    private List<Word> generateWordFrequencyList(String[] wordList){
        return Arrays.stream(wordList)
                .map(word -> new Word(word, SINGLE_WORD_FREQUENCY))
                .collect(Collectors.toList());
    }

    private Map<String, List<Word>> recordWordFrequency(List<Word> wordFrequencyList) {
        Map<String, List<Word>> map = new HashMap<>();
        wordFrequencyList.forEach(wordFrequency ->
                map.computeIfAbsent(wordFrequency.getWord(), word -> new ArrayList<>()).add(wordFrequency)
        );
        return map;
    }
    private String generateWordFrequencyResult(Map<String, List<Word>> map){
        return map.entrySet().stream()
                .map(entry -> new Word(entry.getKey(), entry.getValue().size()))
                .sorted(Comparator.comparing(Word::getWordCount).reversed())
                .map(w -> w.getWord() + " " + w.getWordCount())
                .collect(Collectors.joining("\n"));
    }

}
