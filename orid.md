###1.Key learning, skills and experience acquired
- 1). This morning I learnt three design patterns: Command Pattern, Strategy Pattern and Observer Pattern.

- Command Pattern: The Command Pattern allows you to abstract the relevant operations in a system into commands, separating the caller from the implementer.

- Strategy Pattern: The Strategy Pattern defines a set of algorithms or strategies, and encapsulates each algorithm in a separate class, making them interchangeable.

- Observer Pattern: defines a one-to-many dependency relationship between objects, so that when the state of an object changes, all dependent objects are notified and automatically updated.

- My understanding of the Command Pattern was a little off, which led to a slight error in the diagram I drew for my report, so I'm going to try to understand it more and correct it.

- 2). In the afternoon, I learnt about code refactoring and how to refactor code, which inspired me to pay attention to standardising the naming of code, and to extract repetitive code into functions, and so on.

2) Learned the observer pattern, the observer pattern is a very important design pattern, many open source libraries use this pattern. After learning how to use it, I want to apply more design patterns to my daily development.
   
###2.Problem / Confusing / Difficulties

- I don't know much about Command Patterns，so I'm going to learn more about them and try to apply them to my projects.

###3.Other Comments / Suggestion

- I need to learn more.
